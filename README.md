# ics-ans-role-gnome-desktop

Ansible role to install gnome-desktop.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gnome-desktop
```

## License

BSD 2-clause
