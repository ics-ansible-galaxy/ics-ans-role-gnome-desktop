import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_is_gnome_installed(host):
    package_gnome = host.package('gnome-desktop3')

    assert package_gnome.is_installed
